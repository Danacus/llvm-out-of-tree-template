{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }: 
    flake-utils.lib.eachDefaultSystem (system: let
      llvmpkgs = pkgs.llvmPackages_16;
      pkgs = nixpkgs.legacyPackages.${system};
      clang = llvmpkgs.clang;
      buildDeps = [clang pkgs.cmake pkgs.ninja];
      deps = (with llvmpkgs; [libllvm libclang]);
      extraDevDeps = (with pkgs; [python311 ccache]);
    in {
      devShells = let 
        runOpt = pkgs.writeShellScriptBin "run-opt" ''
          ${llvmpkgs.llvm}/bin/opt \
            -load-pass-plugin=./install/lib64/LLVMHelloWorld.so \
            "$@"
        '';
      in {
        default = pkgs.mkShell {
          packages = buildDeps ++ deps ++ extraDevDeps ++ [runOpt];
        };
      };
      defaultPackage = pkgs.stdenv.mkDerivation {
        pname = "llvm-out-of-tree-template";
        version = "0.0.1";
        src = nixpkgs.lib.cleanSource ./.;

        cmakeFlags = [
          "-DCMAKE_BUILD_TYPE=Debug"
          "-DCMAKE_CXX_COMPILER=${clang}/bin/clang++"
          "-DCMAKE_C_COMPILER=${clang}/bin/clang"
        ];

        nativeBuildInputs = buildDeps;
        buildInputs = deps;
      };
    });
}
