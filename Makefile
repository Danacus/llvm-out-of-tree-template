CMAKE_FLAGS =

-include Makefile.local

MAKEFILE_DIR = $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

BUILD_TYPE ?= Debug
JOBS       ?= 16

BUILDDIR     ?= $(MAKEFILE_DIR)build
INSTALLDIR   ?= $(MAKEFILE_DIR)install
SRCDIR 		 ?= $(MAKEFILE_DIR)src

CMAKE_GENERATOR ?= Ninja
 
#############################################################################

RM = rm -rf
MKDIR = mkdir -p
CMAKE = cmake
NICE = nice
LN = ln -s

#############################################################################

CMAKE_FLAGS += -G "$(CMAKE_GENERATOR)"
CMAKE_FLAGS += -S $(MAKEFILE_DIR)
CMAKE_FLAGS += -DCMAKE_EXPORT_COMPILE_COMMANDS=1
CMAKE_FLAGS += -DCMAKE_CXX_COMPILER=clang++ 
CMAKE_FLAGS += -DCMAKE_C_COMPILER=clang
CMAKE_FLAGS += -DCMAKE_C_COMPILER_LAUNCHER=ccache -DCMAKE_CXX_COMPILER_LAUNCHER=ccache

DEV_CMAKE_FLAGS = $(CMAKE_FLAGS)
DEV_CMAKE_FLAGS += -B $(BUILDDIR)
DEV_CMAKE_FLAGS += -DCMAKE_INSTALL_PREFIX=$(INSTALLDIR)
DEV_CMAKE_FLAGS += -DCMAKE_BUILD_TYPE=$(BUILD_TYPE)

#############################################################################

.PHONY: all
all:
	@echo BUILD_TYPE=$(BUILD_TYPE)
	@echo JOBS=$(JOBS)
	@echo CMAKE_GENERATOR=$(CMAKE_GENERATOR)

.PHONY: configure-build
configure-build:
	$(MKDIR) $(BUILDDIR)
	$(CMAKE) $(DEV_CMAKE_FLAGS)
	$(RM) $(MAKEFILE_DIR)/compile_commands.json
	$(LN) $(BUILDDIR)/compile_commands.json $(MAKEFILE_DIR)

.PHONY: build
build:
ifneq ($(CMAKE_GENERATOR), Ninja)
	$(CMAKE) --build $(BUILDDIR) -- -j$(JOBS)
else
	$(CMAKE) --build $(BUILDDIR)
endif

.PHONY: install
install:
	$(CMAKE) --build $(BUILDDIR) --target install

.PHONY: clean
clean:
	$(RM) $(BUILDDIR)
	$(RM) $(INSTALLDIR)
	$(RM) $(MAKEFILE_DIR)/compile_commands.json

